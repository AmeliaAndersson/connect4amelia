﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


namespace ConnectFour
{
	public class HomeScreenManager : MonoBehaviour {

		//a class for loading different levels from the homescreen, also used for the homescreen button in game.

		public void NextLevelButton(int index){
			Application.LoadLevel (index);
		}

		public void NextLevelButton(string levelName){
			Application.LoadLevel (levelName);
		}
	}
}
