﻿using UnityEngine;
using System.Collections;

public class PlayerList : MonoBehaviour {
	// a class for handling a players score, is reset each time youre at a "enter name" screen. Persistent data available in playerprefs file for game.

	string currentOne, currentTwo;


	// adds current player in case no name is entered
	void Start () {
		PlayerPrefs.DeleteAll ();
		currentOne = "CurrentPlayerOne";
		PlayerPrefs.SetString (currentOne, "Player One");
		currentTwo = "CurrentPlayerTwo";
		PlayerPrefs.SetString (currentTwo, "Player Two");
		PlayerPrefs.Save ();
	}

	
	// Saves player ones name from GUI
	public void AddPlayerOne (string name) {
		if (PlayerPrefs.HasKey(name)) {
			PlayerPrefs.SetString(currentOne, name);
		} else {
			PlayerPrefs.SetInt(name, 0);
			PlayerPrefs.SetString(currentOne, name);
		}
		PlayerPrefs.Save ();
	}

	// Saves player twos name from GUI
	public void AddPlayerTwo (string name) {
		if (PlayerPrefs.HasKey(name)) {
			PlayerPrefs.SetString(currentTwo, name);
		} else {
			PlayerPrefs.SetInt(name, 0);
			PlayerPrefs.SetString(currentTwo, name);
		}
		PlayerPrefs.Save ();
	}


		
}
